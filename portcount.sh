#!/bin/bash
# counts ports that have seen traffic on a particular IP
# should really parameterize directories, IPs, filenames, ignore list
# but who has time for that

#tcpdump -n -r $1 | awk '{if ($2 == "IP") print $3"\n"$5;}' | sed 's/://g' | awk 'BEGIN { FS = "." } ; { print $5 } ' | sed '/^$/d' | sort --version-sort | uniq -c 

rm -f portcount.err

# non dns traffic to this host
for i in $(ls pcaps); do
  tcpdump -F filter -n -r pcaps/$i 2>> portcount.err | awk '{if ($5 ~ "192.168.1.99") print $5;}' | sed 's/://g' | awk 'BEGIN { FS = "." } ; { print $5 } ' | sed '/^$/d' | sort -n >> raw_list_o_ports
done

cat raw_list_o_ports | sort -n | tee >(uniq > list_o_ports) |  uniq -c > count_o_ports

# filters out small results
# comment out for all results
#cat count_o_ports | awk 'BEGIN { FS = " " } ; { if ($1 > 10) print $0 } ' > count_o_ports_
#mv count_o_ports_ count_o_ports

rm -f raw_list_o_ports

# move pcaps and counts to server
# keys not set up yet
#scp -r pcaps ec2-user@redweather.net:capture/
#scp count_o_ports ec2-user@redweather.net:capture/

# everything else
#tcpdump not host 192.168.1.99 -n -r $1

