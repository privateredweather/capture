#!/bin/sh
# runs nc which blocks
# timestamps output file when blocking ends
# calls itself 

function recurse {
  # okay to block once running in background
  nc -vv -l -p $1 >> tcpcap/$1
  # waiting for PID doesn't do anything here
  #PID=$!
  #wait $PID
  echo Received at `date` >> tcpcap/$1
  # no need to run in background/separate child
  # once this is started - pass on port number
  recurse $1
}

#rm tcpcap/*
find tcpcap -size 0 -print0 | xargs -0 rm

for i in $(cat count_o_ports | awk '{ print $2; }'); do
  #nc -l -p $i >> tcpcap/$i &
  # need to start each of these in background
  recurse $i &
done
