#!/bin/sh

# This is called from /etc/rc.local to perform the initial setup.

killall famd
killall portmap
#killall tcpdump
fn=$( date +%a )

# push tcpcaps to redweather.net
cp count_o_ports tcpcap
rsync -a --delete tcpcap redweather.net:
rm tcpcap/count_o_ports

# process last set and set up listeners
./portcount.sh
./listenports.sh

# start capture
nohup tcpdump "not port 1701" -i eth0 -w pcaps/$fn 2> capture.err > /dev/null&

